#!/usr/bin/python

import json
import fire
from jinja2 import Environment, FileSystemLoader, select_autoescape

from mailgun import mailgun as mg


class EmailClient:
    TEMPLATE_DIR = "./templates/"

    def send(self, data):
        if self._using_template(data):
            data["html"] = self._template(data)
            del data["template"]  # template is a reserved keyword for Mailgun
        data = json.dumps(data)
        client = mg.Client()
        response = client.send_email(data)
        return response

    def _template(self, data):
        template = data["template"] + ".html"
        parameters = data["parameters"]
        env = Environment(
            loader=FileSystemLoader(searchpath=self.TEMPLATE_DIR),
            autoescape=select_autoescape(["html", "xml"]),
        )
        template = env.get_template(template)
        return template.render(parameters)

    def _using_template(self, data):
        return "template" in data.keys()


if __name__ == "__main__":
    fire.Fire(EmailClient)
