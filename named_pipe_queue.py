import json
import os
import sys
import time

from email_client import EmailClient
from mailgun.exceptions import MissingKeyError


def send(message):
    try:
        data = json.loads(message)
        response = EmailClient().send(data)
    except Exception as err:
        error = f"{str(err)}"
        response = (500, json.dumps({"error": error}))
    if response[0] != 200:
        notify(response)


def notify(response):
    # requeue or notify of error
    print(response)


if __name__ == "__main__":
    pipe_path = "~/pipe"
    if len(sys.argv) == 2:
        pipe_path = sys.argv[1]
    if os.system(f"test -p {pipe_path}") != 0:
        os.mkfifo(pipe_path)
    pp = open(pipe_path)
    while os.system(f"test -p {pipe_path}") == 0:
        message = pp.readline()
        if len(message) > 0:
            message = message.strip()
            send(message)
            time.sleep(2)
