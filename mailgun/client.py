import json
import requests

from .credentials import Credentials
from .exceptions import MissingKeyError
from .response_handler import ResponseHandler


class Client:
    def __init__(self, credentials=Credentials(), response_handler=ResponseHandler):
        self.api_endpoint = (
            f"https://api.mailgun.net/v3/{credentials.domain_name}/messages"
        )
        self.api_key = credentials.api_key
        self.sender = credentials.sender
        self.response_handler = response_handler

    def send_email(self, data):
        data = json.loads(data)
        data = self._add_default_sender(data)
        self.__class__.validate(data)
        print(data)
        response = self._transmit(data)
        return self.response_handler(response).process()

    def _default_from(self):
        return {"from": f"{self.sender}"}

    def _add_default_sender(self, data):
        return dict(list(self._default_from().items()) + list(data.items()))

    def _transmit(self, data):
        return requests.post(self.api_endpoint, auth=("api", self.api_key), data=data)

    def validate(data):
        required_keys_0 = set(["from", "to", "text"])
        required_keys_1 = set(["from", "to", "html"])
        if not (
            required_keys_0.issubset(data.keys())
            or required_keys_1.issubset(data.keys())
        ):
            raise MissingKeyError("Missing required keys")
