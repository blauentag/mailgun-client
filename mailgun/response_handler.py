from requests.exceptions import HTTPError


class ResponseHandler:
    def __init__(self, response):
        self.response = response

    def process(self):
        try:
            self.response.raise_for_status()
        except HTTPError:
            self._notifier()
        return self._results()

    def _notifier(self):
        if self.response.status_code >= 500:
            return print("Re-queue; a Mailgun issue")
        if self.response.status_code == 429:
            return print("Pause email queue; throttle back")
        if self.response.status_code == 413:
            return print("Attention: payload too large attachment")
        if self.response.status_code == 404:
            return print("Attention: check api_endpoint in Credentials")
        if self.response.status_code == 412:
            return print("Attention: unknown client failure")
        if self.response.status_code == 401:
            return print("Attention: check for missing or bad api_key in Credentials")
        if self.response.status_code == 400:
            return print("Attention: missing required parameter in email data")
        # in case undefined error by Mailgun
        self.response.raise_for_status()

    def _results(self):
        try:
            message = self.response.json()
        except:
            message = self.response.text
        return (self.response.status_code, message)
