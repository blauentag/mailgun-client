import os


class Credentials:
    """"""

    def __init__(self, api_key=None, domain_name=None, sender=None):
        self.api_key = api_key or os.getenv("MAILGUN_API_KEY")
        self.domain_name = domain_name or os.getenv("MAILGUN_DOMAIN_NAME")
        self.sender = sender or os.getenv("MAILGUN_SENDER")
