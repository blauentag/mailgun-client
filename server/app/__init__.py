import sys

sys.path.append("..")

import json
from flask import Flask, request, jsonify

from email_client import EmailClient
from mailgun.exceptions import MissingKeyError


def create_app():
    app = Flask(__name__)

    @app.route("/ping", methods=["GET"])
    def ping():
        message = request.args.get("message", "server is up")
        return jsonify({"message": message})

    @app.route("/email", methods=["POST"])
    def email():
        try:
            data = json.loads(request.data)
            ec = EmailClient()
            mg_response = ec.send(data)

            return mg_response[1], mg_response[0]
        except Exception as e:
            print(e)
            return e, 200

    @app.errorhandler(500)
    def server_error(e):
        return jsonify(error=500, text=str(e)), 500

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify(error=404, text=str(e)), 404

    return app
