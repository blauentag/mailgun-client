import pytest

from flask import url_for


def test_200_response(client):
    data = """{ "to": "blauentag@gmail.com", "subject": "test",
                "text": "This is an test email." }"""
    response = client.post(url_for("email"), data=data)
    assert response.status_code == 200
    assert "Queued" in response.json["message"]


def test_400_missing_to_address(client):
    data = """{"to":"","subject":"pw reset",
               "template":"welcome","parameters":{"name":"Jo"}}"""
    response = client.post(url_for("email"), data=data)
    assert response.status_code == 400
    assert "'to' parameter is not a valid address." in response.json["message"]


def test_500_missing_required_key(client):
    data = """{"cc":"user@example.com","subject":"pw reset",
               "template":"welcome","parameters":{"name":"Jo"}}"""
    response = client.post(url_for("email"), data=data)
    assert response.status_code == 500
    assert "error" in response.json.keys()


def test_text_html_template(client):
    data = """{ "to": "blauentag@gmail.com", 
                "subject": "test",
                "html": "html", "text": "what now?",
                "template": "welcome",
                "parameters": { "name": "name" } }"""
    response = client.post(url_for("email"), data=data)
    assert response.status_code == 200
    assert "Queued" in response.json["message"]
