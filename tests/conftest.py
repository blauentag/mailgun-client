# import sys

# import py
# import pytest
# from xprocess import ProcessStarter

# import server

# server_name = 'server.mail_server'


# @pytest.fixture(autouse=True)
# def start_server(xprocess):
#     python_executable_full_path = sys.executable
#     python_server_script_full_path = py.path.local(__file__).dirpath("../server/mail_server.py")

#     class Starter(ProcessStarter):
#         pattern = '*Running*'
#         args = [python_executable_full_path, python_server_script_full_path]

#     xprocess.ensure(server_name, Starter)

#     yield

#     xprocess.getinfo(server_name).terminate()

import pytest
from server.app import create_app


@pytest.fixture
def app():
    app = create_app()
    return app
