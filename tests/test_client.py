import pytest
from mailgun import mailgun as mg


def test_200_response():
    client = mg.Client()
    data = '{ "to": "blauentag@gmail.com", "subject": "test", "text": "This is an test email." }'
    response = client.send_email(data)
    assert response[0] == 200


def test_400_response():
    client = mg.Client()
    data = '{ "to": "", "subject": "test", "text": "This is an test email." }'
    response = client.send_email(data)
    print(f"{response}")
    assert response[0] == 400
    assert response[1] == {
        "message": "'to' parameter is not a valid address. please check documentation"
    }


def test_401_response():
    credentials = mg.Credentials(api_key="y")
    client = mg.Client(credentials)
    data = '{ "to": "blauentag@gmail.com", "subject": "test", "text": "This is an test email." }'
    response = client.send_email(data)
    print(f"{response}")
    assert response[0] == 401
    assert response[1] == "Forbidden"


def test_missing_key_error():
    client = mg.Client()
    data = '{ "cc": "blauentag@gmail.com", "subject": "test", "text": "This is an test email." }'
    with pytest.raises(mg.MissingKeyError):
        response = client.send_email(data)
