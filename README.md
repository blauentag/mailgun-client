# Mailgun Client

Example extensible basic Mailgun client
## Setup
- `git clone https://gitlab.com/blauentag/mailgun-client.git` or `git clone git@gitlab.com:blauentag/mailgun-client.git`

-  create venv with Python 3.9.1
- `pip install -r requirements.txt`
- `pip install -e .`  # installs the local mailgun package

- Set environment variables:
  - MAILGUN_API_KEY  # Mailgun API key
  - MAILGUN_DOMAIN_NAME  # Mailgun domain name
  - MAILGUN_SENDER  # Mailgun default sender address; may also be passed in with JSON data

- Run tests
  - `pytest`

## Part 1:

`python email_client.py send '{ "to": "user@example.com", "subject": "Test!", "text": "This is an test email." }'`

## Part 2:

`python email_client.py send '{"to":"user@example.com","subject":"pw reset","template":"welcome","parameters":{"name":"Jo"}}'`

`python email_client.py send '{"to":"user@example.com","subject":"pw reset","template":"password_reset","parameters":{"name":"Jo","reset_link":"https://www.example.com/reset_password/123"}}'`

And one still may:
`python email_client.py send '{ "to": "user@example.com", "subject": "Test!", "text": "This is an test email." }'`

## Part 3:

The api server and endpont for sending email are covered by pytest in tests/test_server.py

For operation, `python server/mail_server.py`

There is no authentication currently. As I try to timebox this exercise, implementing authentication would take too much time. For an api server, I would -- with time -- implement a token based system. 

Any actual user initated emailing would make the actual /email endpoint call using a token. This could be further secured by only allowing local IPs with the token to make the call. User authetication with role access would control access to the endpoints allowing the user to set up the process to email.

Background processes pulling jobs off an queue would also use the token process ensure use of the api and limit to authorized actors.

## Part 4:

For time constraints, I chose a named pipe as the queue.

Listen to named pipe
`python named_pipe_queue.py ~/pipe`

In other terminals, pass JSON to named pipe to queue up.
`echo '{"to":"user@example.com","subject":"Welcome!","template":"welcome","parameters":{"name":"Jo"}}' > ~/pipe`


Kafka and AWS SNS are pub/sub queues. The queues hold their messages for a length of time and maintain a pointer for each subscriber. The pointer can be moved back to 'replay' older messages. These are useful traits for when an issue arises with the subscriber.

Data loss is an issue for items on the queue already if the queue is strictly on one server and the hard drive crashes. Kafka -- and I believe -- SNS both write directly to disc, so a restart is not an issue for items already enqueued. An advantage to AWS SNS is that it can be distributed across multiple availability zones. So in the event of a catastropic failure in one zone, the other zones still have the data.

Now we have to consider the publisher. Often these can be after commit hooks for saving an object to the database. For example, an user is created, an hook may publish this event to a Kafka topic. If the queue was unreachable, a resiliency job must be in place to log those failures and be able to publish the events when the queue is again available. One way this could be done is to update a flag on the database record that the event was successfully published. Another would be to not the initial failure time and publisher, create a background job. When the queue is again available, not the time in the job, and schedule the job to run.

Specifically the mailgun and this design, in the mailgun.response_handler, I call attention to the possible status codes returned by Mailgun. For any code 500 or larger, this was a failure of Mailgun servers, and these should be effectively requeued and run later. The 400s should raise a notification for operations to step in identify the issue and correct the data being used.